Confessions Android Application Documentation
Table of Contents
1.	Introduction
•	1.1 Purpose
•	1.2 Scope
•	1.3 Audience
•	1.4 Overview
2.	Features
•	2.1 User Registration and Authentication
•	2.2 Creating Confessions
•	2.3 Anonymous Posting
•	2.4 Liking and Commenting
•	2.5 Sorting and Filtering
•	2.6 Notification System
•	2.7 Anonymous Voice and Video Calling
3.	Installation and Setup
•	3.1 Prerequisites
•	3.2 Installation Steps
4.	User Guide
•	4.1 Account Creation
•	4.2 Creating a Confession
•	4.3 Interacting with Confessions
•	4.4 Managing Notifications
•	4.5 Account Settings
5.	Technical Details
•	5.1 Architecture Overview
•	5.2 Technologies Used
•	5.3 Database Schema
•	5.4 API Endpoints
•	5.5 Third-party Libraries
•	5.6 Security Measures
6.	Troubleshooting
•	6.1 Common Issues
•	6.2 Reporting Bugs
7.	Support and Contact
•	7.1 Support Channels
•	7.2 Contact Information
1. Introduction
1.1 Purpose
The "Confessions" Android Application aims to provide users with a platform to express their feelings anonymously and share confessions with others. The application offers a safe and supportive environment for users to connect and interact with each other.
1.2 Scope
This documentation outlines the features, installation instructions, user guide, technical details, troubleshooting, and support information for the "Confessions" Android Application.
1.3 Audience
This documentation is intended for users of the "Confessions" Android Application, as well as developers and administrators who may need to understand the application's functionality and technical aspects.
1.4 Overview
The "Confessions" application allows users to create anonymous confessions, engage with other users' confessions through likes and comments, and customize their notification preferences.
2. Features
2.1 User Registration and Authentication
Users can create accounts using a valid email address or through social media authentication. Account creation requires verification to ensure a secure and trustworthy user base.
2.2 Creating Confessions
Users can create anonymous confessions by composing text and, optionally, attaching images. Users have the choice to tag their confessions with relevant categories.
2.3 Anonymous Posting
Confessions are posted anonymously to protect users' identities and promote open expression of emotions.
2.4 Liking and Commenting
Users can engage with confessions by liking and commenting on them. This interaction fosters a supportive community environment.
2.5 Sorting and Filtering
Confessions can be sorted by recency, popularity, or category, allowing users to discover content that resonates with them.
2.6 Notification System
Users can receive notifications for interactions on their confessions, as well as updates from other users they follow.
2.7 Anonymous Voice and Video Calling
Anyone can call to anyone 
3. Installation and Setup
3.1 Prerequisites
•	Android device running Android OS X.X or higher
•	Stable internet connection
3.2 Installation Steps
1.	Open any Browser.
2.	Search for "Confessions followed by sourceforge tag" and locate the official app.
3.	Click on download and install the application.
4.	Note: Download only latest version for better user experience.
4. User Guide
4.1 Account Creation
•	Launch the "Confessions" app.
•	Click on "Sign Up" or "Register" to create an account.
•	Provide the necessary details, such as username.
•	Complete any verification steps required.
4.2 Creating a Confession
•	Log in to your account.
•	Tap on the "Create Confession" button.
•	Write your confession text.
•	Optionally, select relevant categories for your confession.
•	Click "Post" to publish your confession anonymously.

4.3 Interacting with Confessions
•	Browse through the feed of confessions on the home screen.
•	Tap a confession to view details.
•	Like a confession by clicking the heart icon.
•	Comment on a confession by typing in the comment box and clicking "Submit."
4.4 Managing Notifications
•	Access the "Settings" section within the app.
•	Navigate to "Notification Preferences."
•	Customize the types of notifications you want to receive.
4.5 Account Settings
•	Go to the "Profile" section.
•	Access "Account Settings" to update your profile information, change your username.
5. Technical Details
5.1 Architecture Overview
The "Confession" app follows a client-server architecture, with the Android client interacting with a remote server via RESTful APIs.
5.2 Technologies Used
•	Android SDK
•	Java programming languages
•	RESTful APIs for server communication
•	Firebase Authentication for user authentication
•	Firebase Cloud Firestore for data storage
5.3 Database Schema
•	Users
•	ID, profile picture
•	Confessions
•	ID, text, images, timestamp
•	Likes
•	ID, confession ID, timestamp
•	Comments
•	ID, confession ID, text, timestamp
5.4 API Endpoints
5.5 Third-party Libraries
•	Picasso: Image loading and caching
•	Retrofit: HTTP client for API communication
•	JSON: JSON serialization and deserialization
•	CircleImageView: Circular image view for user profile pictures
5.6 Security Measures
•	User data encryption
•	Secure token-based authentication
•	Regular security updates and code reviews
6. Troubleshooting
6.1 Common Issues
•	Reloading the confessions
•	Notifications not working
6.2 Reporting Bugs
•	If you encounter any technical issues or bugs, please report them through the "Contact Us" section within the app or via email at https://www.sourceforge.net/p/confessions/report
8.	Support and Contact
•	If you are having any issue, contact on our Instagram page: https://www.instagram.com/confession.support/

7.1 Support Channels
For assistance, you can reach out to our support team through the "Contact Us" section within the app or by sending an email to dev.jangramanish@gmail.com .
7.2 Contact Information
•	Website: https://www.sourceforge.net/p/confessions
•	Email: dev.jangramanish@gmail.com
•	Social Media: Follow us on Facebook, Twitter, and Instagram for updates and announcements.
________________________________________This documentation provides an in-depth understanding of the "Confessions" Android Application, including its features, installation, usage guide, technical details, troubleshooting, and support.

